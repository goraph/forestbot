require 'cinch'
require 'cinch-dicebag'
require 'net/http'
require 'uri'
require 'json'
require 'formdata'
require_relative 'plugins/link_info'
require_relative 'plugins/help'

class QuestBookChat
  def initialize(bot)
    @bot = bot
  end
  def start
    while true
      sleep 5
      @bot.handlers.dispatch(:questbook)
    end
  end
end

class QuestBook
  include Cinch::Plugin

  listen_to :questbook
  def listen(m)
    params = FormData.new
    params.append('msg', '')
    params.append('from', "#{Time.now.to_i - 5}00")
    params.append('away', 0)
    req = params.post_request("/forum/chat/message.php")
    http = Net::HTTP.new('quest-book.ru', 443)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    req.add_field 'Referer', 'https://quest-book.ru/forum/chat/'
    req.add_field 'X-Requested-With', 'XMLHttpRequest'
    response = http.request(req)
    response = JSON.parse response.body
    begin
      if response['msgs'] then
        for msg in response['msgs'] do
          who = '[Система]'
          if msg[1] != '' then
            who = "[#{msg[1]}]"
          end
          if msg[2] != @last then
            Channel('#questbook').send("#{who}: #{msg[2]}")
            @last = msg[2]
          end
        end
      end
    rescue NoMethodError
      puts response['msgs'].inspect
    end 
  end
end

bot = Cinch::Bot.new do
  configure do |c|
    c.server = "irc.forestnet.org"
    c.port = 6667
    c.channels = ["#questbook"]
    c.nick = 'Квестбот'
    c.plugins.plugins = [
      Cinch::Plugins::Dicebag,
      Cinch::Help,
      Cinch::LinkInfo,
      QuestBook
    ]
    c.plugins.options[Cinch::LinkInfo] = {
      :blacklist => [/\.xz$/i, /\.zip$/i, /\.rar$/i],
      :no_description => true,
    }
  end

  on :message do |m|
  end
end

bot.loggers.level = :info

Thread.new {
  QuestBookChat.new(bot).start
}

bot.start
