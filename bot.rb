require 'cinch'
require 'cinch-dicebag'
require_relative 'plugins/link_info'
require_relative 'plugins/help'

def translit (text)
  table = {
    "a"=>["А","а"],
    "ä"=>["э"],
    "b"=>["Б","б"],
    "c"=>["Ц", "ц"],
    "ch"=>["Ч", "ч"],
    "cha"=>["ча"],
    "chu"=>["чу"],
    "chi"=>["чи"],
    "cho"=>["чо"],
    "che"=>["че"],
    "chk"=>["чк"],
    "cht"=>["чт"],    
    "chn"=>["чн"], 
    "chs"=>["чс"], 
    "chl"=>["чл"], 
    "d"=>["д"],
    "e"=>["е"],
    "j" => ["й"],
    "f"=>["ф"],
    "g"=>["г"],
    "h"=>["х"],
    "i"=>["и"],
    "ja"=>["я"],
    "ju"=>["ю"],
    "k"=>["к"],
    "l"=>["л"],
    "m"=>["м"],
    "n"=>["н"],
    "o"=>["о"],
    "ö"=>["ё"],
    "p"=>["п"],
    "r"=>["р"],
    "s"=>["з", "с"],
    "sch"=>["ш"],
    "schtsch"=>["щ"],
    "t"=>["т"],
    "u"=>["у"],
    "ü"=>["ю"],
    "v"=>["ф"],
    "w"=>["в"],
    "x"=>["кс"],
    "y"=>["ы"],
    "z"=>["ж"],
    "zh"=>["ж"],
    "zsch"=>["ч"],
    "tzsch"=>["чш"],
    "'"=>["ь"],
  }
  map = table.sort_by {|k,v| v.length <=>  k.length}
  map.each do |translit_key, translit_value|
    text.gsub!(translit_key.capitalize, translit_value.first)
    text.gsub!(translit_key, translit_value.last)
  end
  text
end

bot = Cinch::Bot.new do
  configure do |c|
    c.server = "irc.forestnet.org"
    c.port = 6667
    c.channels = ["#urq", "#instead", "#qsp", "#ifrus"]
    c.nick = 'XyZ'
    c.plugins.plugins = [
      Cinch::Plugins::Dicebag,
      Cinch::Help,
      Cinch::LinkInfo,
    ]
    c.plugins.options[Cinch::LinkInfo] = {
      :blacklist => [/\.xz$/i, /\.zip$/i, /\.rar$/i],
      :no_description => true,
    }
  end

  on :message do |m|
    if m.user.nick == 'MAlischka' or m.user.nick == 'MA' then
      m.reply( translit(m.message) )
    end

  end
end
bot.loggers.level = :info

bot.start
